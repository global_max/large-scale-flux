
function TokensManagerExtension(instance, sandbox, core){

	var instanceId = sandbox.id

	instance.sleep = function(){
		sandbox.sub.getChildren().forEach(function(childModule){
			childModule.sleep()
		})
		core.sleepTokens(instanceId)
	}
	instance.wake = function(){
		core.wakeTokens(instanceId)
		sandbox.sub.getChildren().forEach(function(childModule){
			childModule.wake()
		})
	}		
}

module.exports = TokensManagerExtension
