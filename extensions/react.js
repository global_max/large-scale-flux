var React = require('react')

//mixins
//var IntlMixin = require('./react-intl-mixin')

var ReactIntl = require('react-intl')
var IntlMixin = ReactIntl.IntlMixin

function ReactExtension(instance, sandbox, core, options){

	//creating React element
	var ReactComponentClass

	if (typeof instance.component != 'undefined'){
		if (!instance.component.mixins) instance.component.mixins = []		
		instance.component.mixins.push(IntlMixin)
	}

	if (instance.component){			
		ReactComponentClass = React.createClass(instance.component)
	}

	instance.getReactElement = function(props){
		if (!ReactComponentClass) 
			throw Error('Core:instantiate : no ReactComponentClass defined')

		return (<ReactComponentClass {...props} messages={core.messages} />)
	}
}

module.exports = ReactExtension