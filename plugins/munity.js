var munityRoutes = require('munity/routes')

function MunityPlugin (options){

	var core = this
	
	//export the core only the root munity module
	if (moduleName === 'Munity') {
		this.Sandbox.prototype.core = core		
	}

	var moduleMatch = munityRoutes.moduleMapper(instanceId)

	if (moduleMatch == moduleName){
		this.Sandbox.prototype.route = munityRoutes.matchRoute(moduleName, instanceId)
		this.Sandbox.prototype.params = munityRoutes.extractParams(this.route, instanceId)
	}	
}

module.exports = MunityPlugin