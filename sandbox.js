var Core = require('./core')

var munityRoutes = require('munity/routes')

var MembershipModalModule = require('munity/modules/shared/membership-modal')

var Sandbox = function(core, instanceId, options, moduleName){

	this.id = instanceId
	this.moduleName = moduleName
	this.options = options

	//export the core only the root munity module
	if (moduleName === 'Munity') {
		this.core = core		
	}

	var moduleMatch = munityRoutes.moduleMapper(instanceId)

	if (moduleMatch == moduleName){
		this.route = munityRoutes.matchRoute(moduleName, instanceId)
		if (this.route){
			this.params = munityRoutes.extractParams(this.route, instanceId)			
		}
	}

	this.getGlobalModule = core.getGlobalModule

	var sandbox = this

	this.youShouldLogin = function(){

		if (sandbox.isLoggedIn() == false){

			var loginModalModule = sandbox.getGlobalModule('login-modal')

			sandbox.handleAction({
				actionType : 'modal:open',
				modal : loginModalModule.getReactElement
			})

			return true;

		} else {

			return false
		}
	}

	this.youShouldBeMember = function(){

		if (!sandbox.getCurrentMembership()) {

			sandbox.sub.register('MembershipModal', MembershipModalModule)

			var modalModule = sandbox.sub.instantiate('MembershipModal', {
				instanceId : 'membership-modal'
			})

			sandbox.handleAction({
				actionType : 'modal:open',
				modal : modalModule.getReactElement
			})

			return true

		} else {
			
			return false
		}

	}

	this.sub = new this.SubCore({ parentSandbox : this })

	return this
}


module.exports = Sandbox