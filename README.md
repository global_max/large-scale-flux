# Large-scale flux application architecture #

Flux help you develop large-scale javascript application, but what if you have a large-scale flux application?

This is a proposed architecture to do so.
* Large-scale javascript app patterns
* Flux


### Getting started ###

Create an instance of the core:

```
#!javascript
var core = new Core({
	currentUser : currentUser
})
```

Define your module:

```
#!javascript

var RootModule = function (sandbox){

	//module initialization code

	return {

		componenet : {

			render : function(){
				return (<div>Hello, Flux!</div>)
			}
		}

	}
}
```

Register your module at the core
```
#!javascript
core.register('RootModule', RootModule)
```

Create an instance of the module you just registered
```
#!javascript
var root = core.instantiate('RootModule', {
	instanceId : 'root',
	options : {}
})
```
Now you can get the corresponding React element:
```
#!javascript
React.render(root.getReactElement(), document.getElementById('content'))

```



### Core concept ###

The core concept is to use a controller React componenet that can access its own set of objects in the scope of its module, in order to control a subordinate logic-less componenet.


```
#!javascript

//ordinary component

var HelloComponent = React.createClass({
	render : function(){
		return (<div>Hello, {this.props.name}</div>)
	}
})

var HelloModule = function (sandbox){

	//access options passed from initiaiting core
	var options = sandbox.options

	//define your store here
	var store = new Store({})

	sandbox.listen(store.dispatchCallback)

	sandbox.listen(function(payload){
		console.log(payload.actionType)
	})

	sandbox.handleAction({
		actionType : 'moduleInitialized'
	})

	return {

		componenet : {

		    getInitialState : function(){
				return store.getData()
		    },
		    storeListener : function(){
		    	this.setState(store.getData())
		    },
		    componentDidMount: function(){
				store.addChangeListener(this.storeListener)
		    }, 
		    componentWillUnmount : function(){
				store.removeChangeListener(this.storeListener)
		    },
		    customActionHandler : function(arg){
		    	sandbox.handleAction({
					actionType : 'customAction'
					arg : arg
				})
		    },
			render : function(){
				return (<HelloComponent 
					{...this.state} 
					customActionHandler={customActionHandler} />)
			}
		}

	}
}

```


### Sub-modules ###


```
#!javascript

var ChildModule = function ( sandbox ){ ... }

var ParentModule = function (sandbox){

	//parent private scope..

	sandbox.sub.register('ChildModule', ChildModule)
	var childModule = sandbox.sub.instantiate('ChildModule', {
		instanceId : 'child',
		options : { ... }	
	})


	return {

		...

		componenet : {

			var childElement = childModule.getReactElement({
				// ... pass props here	
			})

			render : function(){
				return (
					<div>
						...
						{ childElement } 
						...
					</div>
				)
			}
		}

	}
}
```



### Single Dispatcher ###


```
#!javascript

var FirstModule = function ( sandbox ){ ... }
var SecondModule = function ( sandbox ){ ... }

var ParentModule = function (sandbox){

	//parent private scope..

	sandbox.sub.register('FirstModule', FirstModule)
	var firstModule = sandbox.sub.instantiate('FirstModule', {
		instanceId : 'first',
		options : { ... }	
	})

	sandbox.sub.register('SecondModule', SecondModule)
	var secondModule = sandbox.sub.instantiate('SecondModule', {
		instanceId : 'second',
		options : { ... }	
	})	

	var activeModule = firstModule

	return {

		...

		switchHandler  : function(){

			if (active == 'first'){
				toSwitch = secondModule
			} else {
				toSwitch = firstModule
			}

			activeModule.sleep()
			activeModule = toSwitch()
			activeModule.wake()

		},

		componenet : {

			var activeElement = activeModule.getReactElement({
				// ... pass props here	
			})

			render : function(){
				return (
					<div>
						...
						{ activeElement } 
						...
					</div>
				)
			}
		}

	}
}
```